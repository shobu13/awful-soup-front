import BaseModel from "@/models/baseModel";
import Tag from "@/models/tag";

export enum IngredientType {
  Base = 'A_0',
  Ingredient = 'A_1'
}

export enum NodeType {
  IngredientNode = "IngredientNode"
}

interface User {
  username: string
}

export default class Ingredient extends BaseModel {
  name?: string
  ingredient_type?: IngredientType
  tags?: Array<Tag>
  alt?: Ingredient
  alts?: Array<Ingredient>
  bases?: Array<Ingredient>
  ingredients?: Array<Ingredient>
  definition?: string
  examples?: string

  created_by?: User

  fromApi(node: any) {
    this.id = node.id
    this.name = node.name
    this.ingredient_type = node.ingredientType
    this.tags = node.tags?.edges.map((raw_tag: any) => {
      const tag = new Tag()
      tag.fromApi(raw_tag.node)
      return tag
    })
    if (node.alt) {
      this.alt = new Ingredient()
      this.alt.fromApi(node.alt)
    }
    this.alts = node.alts?.edges.map((raw_alt: any) => {
      const alt = new Ingredient()
      alt.fromApi(raw_alt.node)
      return alt
    })
    this.bases = node.bases?.edges.map((raw_base: any) => {
      const base = new Ingredient()
      base.fromApi(raw_base.node)
      return base
    })
    this.ingredients = node.ingredients?.edges.map((raw_ingredient: any) => {
      const ingredient = new Ingredient()
      ingredient.fromApi(raw_ingredient.node)
      return ingredient
    })
    this.definition = node.definition
    this.examples = node.examples
    this.created_by = node.createdBy
  }

  static encodeId(db_id: number, nodeType: NodeType = NodeType.IngredientNode): string {
    return btoa(nodeType + ':' + db_id)
  }

}

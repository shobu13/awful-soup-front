import BaseModel from "@/models/baseModel";
import Ingredient from "@/models/ingredient";

export enum NodeType{
  TagNode = "TagNode"
}

export default class Tag extends BaseModel {
  name?: string
  color?: string
  ingredients?: Array<Ingredient>

  fromApi(node: any) {
    this.id = node.id
    this.name = node.name
    this.color = node.color
    this.ingredients = node.ingredients?.edges.map((raw_ingredient: any) => {
      const ingredient = new Ingredient()
      ingredient.fromApi(raw_ingredient.node)
      return ingredient
    })
  }

  static encodeId(db_id: number, nodeType: NodeType=NodeType.TagNode): string {
    return btoa(nodeType + ':' + db_id);
  }
}

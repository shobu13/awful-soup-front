export function stringField(input: string | undefined, name: string, format?: (i: string) => string): string {
  if (input) {
    return `${name}:"${format ? format(input) : input}",`
  }
  return ''
}

export function numberField(input: number | undefined, name: string, format?: (i: number) => string): string {
  if (input) {
    return `${name}:${format ? format(input) : input},`
  }
  return ''
}

export function enumField(input: any | undefined, name: string, format?: (i: any) => string): string {
  if (input) {
    return `${name}:${format ? format(input) : input},`
  }
  return ''
}

export function arrayField(input: Array<any> | undefined, name: string, format?: (i: Array<any>) => string): string {
  if (input) {
    return `${name}:[${format ? format(input) : input}],`
  }
  return ''
}

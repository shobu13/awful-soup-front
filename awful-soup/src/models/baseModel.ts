import {NotImplementedError} from "@/models/errors";

export default abstract class BaseModel {
  id?: string

  /*
  implement a function that take a node raw data and fill the properties of the newly created item with it.
   */
  abstract fromApi(node: any): void

  /*
  implement a function who take a bdd id as number and return the graphql NodeID at the format NodeType:db_id
   */
  static encodeId(db_id: number, nodeType?: string): string {
    throw NotImplementedError
  }


  public getId(): number | undefined {
    return this.id ? +atob(this.id as string).split(':')[1] : undefined
  }
}

export class NotAuthenticatedError extends Error {
  constructor() {
    super();
  }

}

export class TokenExpiredError extends Error {
  constructor() {
    super();
  }

}

export class InvalidRefreshToken extends Error {
  constructor() {
    super();
  }

}

import Ingredient, {IngredientType} from "@/models/ingredient";
import Tag from "@/models/tag";

export default class NewWordForm {
  id?: number
  name?: string
  type?: IngredientType
  definition?: string
  examples?: string
  alt?: Ingredient
  tags?: Array<Tag>
  bases?: Array<Ingredient>

  parse(): Ingredient {
    const ingredient = new Ingredient()
    ingredient.id = this.id ? btoa("IngredientNode:" + this.id) : undefined
    ingredient.name = this.name
    ingredient.ingredient_type = this.type
    ingredient.definition = this.definition
    ingredient.examples = this.examples

    ingredient.alt = this.alt
    ingredient.tags = this.tags
    ingredient.bases = this.bases

    return ingredient
  }
}

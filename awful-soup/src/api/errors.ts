export class GraphQLQueryError extends Error {
  errors: Array<any>

  constructor(errors: Array<any>) {
    super()
    this.errors = errors
  }
}

export class GraphQLMutationError extends GraphQLQueryError {
  constructor(errors: Array<any>) {
    super(errors);
  }

}

export const ERROR_MAP: { [key: string]: (e: string) => string } = {
  'UNIQUE constraint failed: core_ingredient.name': (e: string) => 'Ingredient name already exist'
}

export function mapError(e: string): string {
  return ERROR_MAP[e] ? ERROR_MAP[e](e) : e
}


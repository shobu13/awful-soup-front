import BaseConnector from "@/api/baseConnector";
import Tag from "@/models/tag";
import Ingredient from "@/models/ingredient";

export default class TagConnector extends BaseConnector<Tag> {
  async retrieve(query: string): Promise<Tag> {
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: query
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    const tag = new Tag()
    tag.fromApi(result.data.tag)
    return tag
  }

  async list(query: string): Promise<Array<Tag>> {
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: query
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return result.data.allTags.edges.map((raw_tag: any) => {
      const tag = new Tag()
      tag.fromApi(raw_tag.node)
      return tag
    });
  }

  async mutate(data: Tag): Promise<number> {
    return 0
  }
}

export const TAG_LIST_FIELDS = `
        id
        color
        name
        ingredients{
          edges {
              node {
                  id
              }
          }
        }`

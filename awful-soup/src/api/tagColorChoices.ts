import axios from "axios";
import {debug} from "util";

export default class TagColorChoices {
  private static instance: TagColorChoices
  private choices: { [name: string]: string } = {}

  private constructor() {
  }

  public static async getInstance(): Promise<TagColorChoices> {
    const response = await axios.post(import.meta.env.VITE_API_URL + `/graphql`, {
      query: `query {
              __type(name: "CoreTagColorChoices"){
                enumValues{
                  name
                  description
              }
            }
        }`
    }, {
      headers: {
        'Content-Type': 'application/json'
      }
    })

    if (!TagColorChoices.instance) {
      TagColorChoices.instance = new TagColorChoices()
      response.data.data.__type.enumValues.forEach((value: any) => {
        TagColorChoices.instance.choices[value.name] = value.description
      })
    }

    return TagColorChoices.instance
  }

  public get(name: string | undefined){
    return this.choices[name as string]
  }

  public reverse(value: string){
    for (let i in this.choices){
      if (this.choices[i] === value){
        return i
      }
    }
  }
}
